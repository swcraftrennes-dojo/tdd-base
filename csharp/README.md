# How to set up 
Install VS Code
# Where do i write my tests ?
In Tests/TestClass.cs
# Where do i write my class to be tested ?
In a .cs file that you can put in Tests/Src
# How do i run my tests ?
Open the tests dir with VS Code, then in VS Code's terminal window, type :
```bash
dotnet test
```
