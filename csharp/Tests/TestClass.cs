using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TestClass
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void ThisIsATest()
        {
            Assert.That(3+3, Is.EqualTo(6));
        }
    }
}
