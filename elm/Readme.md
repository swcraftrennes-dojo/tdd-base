# Elm TDD code base

## Setup environment

* [Install Elm](https://guide.elm-lang.org/install/elm.html)
* Install elm-test: `npm install elm-test`

## Run tests

Run tests once: `elm-test`

Run tests continously: `elm-test --watch`
 
