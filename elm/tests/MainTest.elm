module MainTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Main exposing (hello)
import Test exposing (..)


suite : Test
suite =
    describe "hello function"
        [ test "should return Hello, World" <|
            \_ ->
                Expect.equal "Hello, World!" hello
        ]
