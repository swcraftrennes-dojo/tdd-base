# Golang TDD base code

Uses ginkgo with [gomega](https://onsi.github.io/gomega/#provided-matchers) framework to describe tests.

Write your code in a new `.go` file (package `dojo`), add tests in `dojo_test.go`.

Run tests with `go test` (or for more verbose output, `ginkgo -v`).
