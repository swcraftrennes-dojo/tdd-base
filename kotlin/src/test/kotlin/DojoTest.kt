import kotlin.test.Test
import kotlin.test.assertEquals

class DojoTest {

    @Test
    fun `should be equal to 'Hello world !'`() {
        assertEquals("Hello world !", Dojo().hello())
    }
}