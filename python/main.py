import time

from src.connectors.downloadFile import downloadStations
from src.connectors.mysql import Mysql
from config import config

database = Mysql(config['database'])

while 1:
    print('iteration, downloading')
    stations = downloadStations('https://opendata.paris.fr/explore/dataset/velib-disponibilite-en-temps-reel/download/?format=json&timezone=Europe/Berlin')
    database.Stations.set(stations)
    time.sleep(config['delay'])
